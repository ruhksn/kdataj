---
title: "A00288671 - Assignment 3"
author: "Ruairi Hickson"
date: "11/24/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
```

# Data Processing and Cleansing

After some rudimentary text file exploration, we can see the data from Soil_data.csv has an intro line and a blank line after that.
We can also see the data has the column name and, where it exists, the dimension of that row as well (e.g. % or mg/kg).
The first thing we're going to do is read in the third and fourth rows of data, capturing the column names and the dimensions in a data frame. We're then going to combine these into a single character vector in the format <NAME> <Dimension>

```{r}
df <- read.csv("Soil_data.csv",skip=2,nrow=2)
str(df)
```

We concatenate the column names with the dimensions and trim strings of whitespace where it is a concern. We use the paste0() function to concatenate instead of paste, as this doesn't introduce a gap between concatenated elements.

```{r}
dfcolnames <- paste0(str_trim(df[1,])," ",str_trim(df[2,]))
dfcolnames <- str_trim(dfcolnames)
dfcolnames
```
Looks good. Now Let's read the CSV again just for the data. We skip the first four rows and put the rest in a dataframe.

```{r}
df <- read.csv("Soil_data.csv"
               ,skip=4
               ,stringsAsFactors = F)
```

Now we assign the column names we extracted previously to the columns of the new dataframe:
```{r}
colnames(df) <- dfcolnames
```

And verify the data is in place the way we want it.
```{r}
str(df)
```

Though we could remove some columns here, such as Sample_No and Sample_ID, we opt not to as they are not interfering with anything downstream of here.

However if we want to, we could use the following commented-out code as an example
```{r}
df <- df[!names(df) %in% c("Sample_No")]
```

From column 12 on, there exist character columns that should be numeric values. If we fail to coerce them, this will cause problems for our analysis. We use a for loop to coerce these columns into numeric

```{r}
i <- 12

for (i in 12:(length(df))) {
  df[,i] <- as.numeric(df[,i])
}

str(df)
```

So now all our numeric columns are actually numeric, but we have introduced NAs by coercion. We check for NAs in the data. The following function will expose the names of the columns containing NAs.
```{r}
names(which(sapply(df,anyNA)))
```

So there are still columns with NA values. We will impute the mean value of the column into all NAs to smooth over the data. Though we may be affecting some real values, there is a chance replacing all NAs with zeroes corrupts any patterns we may look for. Also, removing any rows with NA values may corrupt patterns in the other variables in the dataset.

We use a for loop again to remove NAs and replace them with the mean value of that column.
```{r}
i<-12

for(i in 12:length(df)){
  df[is.na(df[,i]), i] <- mean(df[,i], na.rm = TRUE)
}
```

Let's check again for any remaining NA values (this output should be empty)
```{r}
names(which(sapply(df,anyNA)))
```

Now that we're free of NAs, let's get a summary of the dataset and ee what the data we're dealing with looks like.
```{r}
summary(df)
```


# 1: Show how the pH results compare for the different types of land use that have been recorded.

We're going to start with a simple scatter plot with pH level on the y-axis and Type of Land (LandUse) on the x-axis. Different types of land will be coloured-in for visual clarity.

```{r}
dfpHLandTypes <- df[c("LandUse","pH")]

ggplot(dfpHLandTypes) +
  geom_point(aes(x=reorder(LandUse,pH)
               ,y=pH
               ,color = LandUse)
           ,stat = "identity") +
  theme_minimal() +
  xlab("Type of Land") +
  ylab("pH Level") +
  labs(title = "pH by Type of Land") +
  theme(legend.position="none")
```

As can be seen from the scatter plot, there is a fairly good distribution of values across all land types. Notably, "Boggy" only has one value. This is likely meant to be classed as "Bog" and was probably a data entry error. We will reclassify this value to "Bog" and get an average pH level for each of the land types using a bar chart.

```{r}
df[which(df$LandUse == "Boggy"),]$LandUse <- "Bog"

dfpHLandTypes <- df %>%
  group_by(LandUse) %>%
  summarise(pH = mean(pH)) %>%
  arrange(desc(pH)) %>% 
  as.data.frame()

ggplot(dfpHLandTypes) +
  geom_bar(aes(x=reorder(LandUse,pH)
               ,y=pH
               ,fill = LandUse)
           ,stat = "identity") +
  geom_text(aes(x=reorder(LandUse,pH)
                ,y=pH
                ,label = round(pH,2))
            ,vjust = 1.5
            , colour = "black") +
  theme_minimal() +
  xlab("Type of Land") +
  ylab("pH Level") +
  labs(title = "pH by Type of Land") +
  theme(legend.position="none"
        ,axis.text.y = element_blank())
```
The above chart tells us that "Bog" has the lowest average pH levels, while "Tillage" has the highest average.


# 2: What's my Element? - Investigate if the prevalence of your assigned element is different in samples taken from different areas of land use.

Here, we parse my Student ID (A00288671) and add mg/kg to the end of the element name to see what it is. We can see from reading the csv file that my Element is Cr (Chromium), probably based on the family of web browsers I use. I will assign this element to a variable for referencing.
```{r}
df1 <- read.csv("studentAssign.csv")
df1$Element <- paste0(df1$Element," mg/kg")

# Assigning the relevant Element to the variable "myElement"
myElement <- colnames(df[names(df) %in% df1[which(df1$StudentID == 'A00288671'),]$Element])
```

Now let's see how Chromium prevails in different land types. We can check this out with a boxplot to get an idea of distribution levels, averages, and possibly negate potential incorrect readings as outliers.

```{r}
dfmyElement <- df[,c("LandUse",myElement)]
summary(dfmyElement)
head(dfmyElement)

ggplot(dfmyElement) + 
  geom_boxplot(aes(x = LandUse
                   , y = `Cr mg/kg`
                   , fill = LandUse))

dfmyElementStats <- dfmyElement %>%
  group_by(LandUse) %>%
  summarise(`Number of Samples` = n()
            ,`Average Chromium Levels mg/kg` = mean(`Cr mg/kg`)
            ,`Max Chromium Levels mg/kg` = max(`Cr mg/kg`)
            ,`Min Chromium Levels mg/kg` = min(`Cr mg/kg`)
            ,`Range Chromium Levels mg/kg` = max(`Cr mg/kg`)-min(`Cr mg/kg`)
            ,`Interquartile Range Chromium Levels mg/kg` = quantile(`Cr mg/kg`,c(0.75))-quantile(`Cr mg/kg`,c(0.25))) %>%
  as.data.frame()

dfmyElementStats
```

The prevalence of Chromium in the soil appears to correlate the same as pH levels in the soil (Bog < Forest < Rough < Grass < Tillage). The greatest number of samples by far were taken from Grass (851), and Grass also had the highest floor of Chromium levels recorded (8.6 mg/kg), while Tillage had the highest recorded value (221.7 mg/kg). Tillage also had the highest average Chromium level (56.71962 mg/kg)

The range of values of Chromium levels is also included, but a more reliable basis for the distribution is the Interquartile range, which shows Bog had the widest central distribution (41.01197 mg/kg).


# 3: Investigate how the prevalence of your assigned element compares with other elements in the data (choose five other elements with the same unit of measurement for comparison).

Let's create a dataset that compares Chromium to other elements in the soil data. We need to select comparable features in the dataset. The below code will subset the dataframe to show only features that measure "mg/kg", then selects the first six columns that match that criteria.

```{r}
dfmyElementAndOthers <- df %>%
  relocate(eval(myElement)) %>% 
  select(
    contains(
      substr(
        myElement,str_length(myElement)-1,str_length(myElement)
        )
      )
    ) %>% select(1:6)

dfmyElementAndOthers <- dfmyElementAndOthers %>%
  gather('element','value')

ggplot(dfmyElementAndOthers) +
  geom_point(aes(y = element, x = value))
```
The above scatter plot shows the distribution of samples of five other elements as well as Chromium in the data. Barium has the highest recorded range and value of mg/kg, while Cadmium has the lowest range.


# 4: Investigate the relationship between pH and the amount of your assigned element.
```{r}
dfpHmyElement <- df[,c("pH",myElement)]
summary(dfpHmyElement)

# See a sample of the model dataset
head(dfpHmyElement)

ggplot(dfpHmyElement) +
  geom_line(aes(x=pH,y=get(myElement)))

dfpHmyElementAdj <- dfpHmyElement %>% 
  group_by(pH) %>% 
  summarise(myElement = mean(get(myElement))) %>% 
  as.data.frame()

ggplot(dfpHmyElementAdj) +
  geom_line(aes(x=pH,y=myElement)) +
  geom_smooth(aes(x=pH,y=myElement),method="lm")
```

There appears to be a generally linear relationship between the pH level and the level of Chromium level in the soil, though it is not clear from the first line chart. To make it a bit clearer, I took the mean Chromium level for every pH reading to smooth out the line, resulting in the second line chart. I have also added in a trendline to show the general proportionally-increasing relationship.


# 5: Investigate how well simple linear regression can predict the amount of your element present in a sample based on the pH of the soil.

```{r}
model <- lm(myElement ~ pH, dfpHmyElementAdj)

summary(model)

plot(model)
```

A simple linear regression model on the smoothed data reveals there is a (not-particularly-strong) relationship between pH and Chromium in the soil - shown by the Multiple R-squared value of 0.4133. pH does not seem to be an especially good predictor of Chromium level in the soil. Though it is possible that pH used in combination with another variable could help predict Chromium.

# Reflection

The primary challenge involved in this task was in cleansing the data. The two unused rows at the beginning, and the feature names and measures being split across two rows also added complications. After that, there were still inaccurate values (e.g. "<0.01') that were coercing numeric vectors to characters. After reversing the coercion, these values themselves were eliminated, and the NAs were handled with smoothing.

Even after that, there were still somewhat hidden challenges within the data, such as one of the land types, “Boggy”, only showing up once, presumably being a data entry error meant to classified as “Bog”. Though the impact of including/discarding this row may have been minimal, it may have been indicative of other undiscovered gotchas within the data.

The analysis itself was pretty straightforward, comparisons of elements between each other, against land types, against pH levels. I included column charts, line charts, box plots, and scatter plots in my analysis. While there may have been use for other types of visualisation, I feel these core ones covered the bulk of the questions being asked. There was no practical use here for a pie chart, for example.

Running a linear regression model on the pH value to try and predict levels of Chromium in the soil was interesting, although it was always unlikely that a single-variable model would provide strong predictive capability, it would be interesting with more time and incentive to have incorporated other features in the model, and see if there is a way to more accurately predict Chromium levels in the soil, even split out the basic model against different soil types.

Finally, developing the output in a workbook like RMarkdown is a great way to show findings and thoughts. The workflow capability of the software is great, and if combined with newer libraries like Hugo, would be really nice to publish online as a catalogue of M.Sc. projects.