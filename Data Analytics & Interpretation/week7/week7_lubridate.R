setwd("./week7/")
#install.packages(lubridate)

#Question 1 - two functions to see current date and datetime
library(lubridate)
?lubridate
today()
now()

#Question 2 - use difftime to calculate difference in weeks between two different time formats
x <- "11th april 2012"
y <- "April 24th 2018 11:59:59"

difftime(mdy_hms(y),dmy(x),units="weeks")

#Question 3 - difference between current time and x in hours
difftime(now(),dmy(x),units="hours")

#Question 4 - Add one day to x
x <- "11th april 2018 11.33.00"
x <- dmy_hms(x) + days(1)
x

#Question 5 - Add 100 hours and findout the datetime
x <- x + hours(100)
x

#Question 6 - Difference between 10 year duration and 10 year subtraction
x - years(10)
x - dyears(10)

#dyears subtracts exactly 10 years in consistent values of seconds and does not take into account leap values