#install.packages("DT")
library(DT)
df <- read.csv("./Advanced Databases/plsql.csv")
str(df)
datatable(df, class = 'cell-border stripe'
          , rownames = FALSE
          , caption = 'This is for KDATAJ Advanced Databases Class of 2021'
          , filter = 'top')
